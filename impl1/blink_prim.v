// Verilog netlist produced by program LSE :  version Diamond (64-bit) 3.11.0.396.4
// Netlist written on Sat Mar 19 10:39:00 2022
//
// Verilog Description of module blink
//

module blink (led, clk) /* synthesis syn_module_defined=1 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(1[8:13])
    output led;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(1[26:29])
    input clk;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(1[37:40])
    
    wire GND_net /* synthesis RESET_NET_FOR_BUS20=20, DSPPORT_20=RST3 */ ;
    wire VCC_net /* synthesis CE_NET_FOR_BUS20=20, DSPPORT_20=CE3 */ ;
    wire clk_c /* synthesis DSPPORT_20=CLK3, CLOCK_NET_FOR_BUS20=20, is_clock=1, SET_AS_NETWORK=clk_c */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(1[37:40])
    
    wire led_c;
    wire [24:0]counter;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(3[14:21])
    
    wire n217, n259, n265, n263, n215, n106, n107, n108, n109, 
        n110, n111, n112, n113, n114, n115, n116, n117, n118, 
        n119, n120, n121, n122, n123, n124, n125, n126, n127, 
        n128, n129, n130, n289, n285, n283, n281, n229, n228, 
        n227, n226, n225, n224, n223, n222, n221, n220, n219, 
        n218, n295, n297;
    
    VHI i2 (.Z(VCC_net));
    OB led_pad (.I(led_c), .O(led));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(1[26:29])
    LUT4 i134_4_lut (.A(n283), .B(n297), .C(n295), .D(n259), .Z(n217)) /* synthesis lut_function=(!(A+(B+(C+(D))))) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i134_4_lut.init = 16'h0001;
    LUT4 i1_2_lut (.A(counter[2]), .B(counter[9]), .Z(n263)) /* synthesis lut_function=(A+(B)) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i1_2_lut.init = 16'heeee;
    FD1S3JX counter_43__i24 (.D(n106), .CK(clk_c), .PD(n217), .Q(counter[24])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i24.GSR = "ENABLED";
    IB clk_pad (.I(clk), .O(clk_c));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(1[37:40])
    GSR GSR_INST (.GSR(VCC_net));
    LUT4 i1_4_lut (.A(counter[0]), .B(counter[3]), .C(counter[1]), .D(counter[15]), 
         .Z(n289)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i1_4_lut.init = 16'hfffe;
    LUT4 i1_2_lut_adj_1 (.A(led_c), .B(n217), .Z(n215)) /* synthesis lut_function=(!(A (B)+!A !(B))) */ ;
    defparam i1_2_lut_adj_1.init = 16'h6666;
    FD1S3IX counter_43__i0 (.D(n130), .CK(clk_c), .CD(n217), .Q(counter[0])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i0.GSR = "ENABLED";
    FD1S3AX led_11 (.D(n215), .CK(clk_c), .Q(led_c));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(4[10] 12[6])
    defparam led_11.GSR = "ENABLED";
    FD1S3IX counter_43__i23 (.D(n107), .CK(clk_c), .CD(n217), .Q(counter[23])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i23.GSR = "ENABLED";
    FD1S3JX counter_43__i22 (.D(n108), .CK(clk_c), .PD(n217), .Q(counter[22])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i22.GSR = "ENABLED";
    FD1S3JX counter_43__i21 (.D(n109), .CK(clk_c), .PD(n217), .Q(counter[21])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i21.GSR = "ENABLED";
    FD1S3JX counter_43__i20 (.D(n110), .CK(clk_c), .PD(n217), .Q(counter[20])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i20.GSR = "ENABLED";
    FD1S3JX counter_43__i19 (.D(n111), .CK(clk_c), .PD(n217), .Q(counter[19])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i19.GSR = "ENABLED";
    FD1S3JX counter_43__i18 (.D(n112), .CK(clk_c), .PD(n217), .Q(counter[18])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i18.GSR = "ENABLED";
    FD1S3IX counter_43__i17 (.D(n113), .CK(clk_c), .CD(n217), .Q(counter[17])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i17.GSR = "ENABLED";
    FD1S3JX counter_43__i16 (.D(n114), .CK(clk_c), .PD(n217), .Q(counter[16])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i16.GSR = "ENABLED";
    FD1S3IX counter_43__i15 (.D(n115), .CK(clk_c), .CD(n217), .Q(counter[15])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i15.GSR = "ENABLED";
    FD1S3JX counter_43__i14 (.D(n116), .CK(clk_c), .PD(n217), .Q(counter[14])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i14.GSR = "ENABLED";
    FD1S3JX counter_43__i13 (.D(n117), .CK(clk_c), .PD(n217), .Q(counter[13])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i13.GSR = "ENABLED";
    FD1S3JX counter_43__i12 (.D(n118), .CK(clk_c), .PD(n217), .Q(counter[12])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i12.GSR = "ENABLED";
    FD1S3JX counter_43__i11 (.D(n119), .CK(clk_c), .PD(n217), .Q(counter[11])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i11.GSR = "ENABLED";
    FD1S3IX counter_43__i10 (.D(n120), .CK(clk_c), .CD(n217), .Q(counter[10])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i10.GSR = "ENABLED";
    FD1S3IX counter_43__i9 (.D(n121), .CK(clk_c), .CD(n217), .Q(counter[9])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i9.GSR = "ENABLED";
    FD1S3IX counter_43__i8 (.D(n122), .CK(clk_c), .CD(n217), .Q(counter[8])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i8.GSR = "ENABLED";
    FD1S3IX counter_43__i7 (.D(n123), .CK(clk_c), .CD(n217), .Q(counter[7])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i7.GSR = "ENABLED";
    FD1S3JX counter_43__i6 (.D(n124), .CK(clk_c), .PD(n217), .Q(counter[6])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i6.GSR = "ENABLED";
    FD1S3IX counter_43__i5 (.D(n125), .CK(clk_c), .CD(n217), .Q(counter[5])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i5.GSR = "ENABLED";
    FD1S3IX counter_43__i4 (.D(n126), .CK(clk_c), .CD(n217), .Q(counter[4])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i4.GSR = "ENABLED";
    FD1S3IX counter_43__i3 (.D(n127), .CK(clk_c), .CD(n217), .Q(counter[3])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i3.GSR = "ENABLED";
    FD1S3IX counter_43__i2 (.D(n128), .CK(clk_c), .CD(n217), .Q(counter[2])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i2.GSR = "ENABLED";
    FD1S3IX counter_43__i1 (.D(n129), .CK(clk_c), .CD(n217), .Q(counter[1])) /* synthesis syn_use_carry_chain=1, REG_OUTPUT_CLK=CLK3, REG_OUTPUT_CE=CE3, REG_OUTPUT_RST=RST3 */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43__i1.GSR = "ENABLED";
    LUT4 i1_4_lut_adj_2 (.A(counter[5]), .B(n289), .C(counter[13]), .D(counter[24]), 
         .Z(n295)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i1_4_lut_adj_2.init = 16'hfffe;
    LUT4 i1_2_lut_adj_3 (.A(counter[18]), .B(counter[7]), .Z(n259)) /* synthesis lut_function=(A+(B)) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i1_2_lut_adj_3.init = 16'heeee;
    LUT4 i1_2_lut_adj_4 (.A(counter[11]), .B(counter[12]), .Z(n265)) /* synthesis lut_function=(A+(B)) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i1_2_lut_adj_4.init = 16'heeee;
    LUT4 i1_4_lut_adj_5 (.A(counter[21]), .B(counter[22]), .C(counter[6]), 
         .D(counter[4]), .Z(n281)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i1_4_lut_adj_5.init = 16'hfffe;
    VLO i1 (.Z(GND_net));
    CCU2C counter_43_add_4_25 (.A0(counter[23]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[24]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n229), .S0(n107), .S1(n106));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_25.INIT0 = 16'h555f;
    defparam counter_43_add_4_25.INIT1 = 16'h555f;
    defparam counter_43_add_4_25.INJECT1_0 = "NO";
    defparam counter_43_add_4_25.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_23 (.A0(counter[21]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[22]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n228), .COUT(n229), .S0(n109), .S1(n108));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_23.INIT0 = 16'h555f;
    defparam counter_43_add_4_23.INIT1 = 16'h555f;
    defparam counter_43_add_4_23.INJECT1_0 = "NO";
    defparam counter_43_add_4_23.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_21 (.A0(counter[19]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[20]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n227), .COUT(n228), .S0(n111), .S1(n110));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_21.INIT0 = 16'h555f;
    defparam counter_43_add_4_21.INIT1 = 16'h555f;
    defparam counter_43_add_4_21.INJECT1_0 = "NO";
    defparam counter_43_add_4_21.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_19 (.A0(counter[17]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[18]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n226), .COUT(n227), .S0(n113), .S1(n112));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_19.INIT0 = 16'h555f;
    defparam counter_43_add_4_19.INIT1 = 16'h555f;
    defparam counter_43_add_4_19.INJECT1_0 = "NO";
    defparam counter_43_add_4_19.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_17 (.A0(counter[15]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[16]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n225), .COUT(n226), .S0(n115), .S1(n114));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_17.INIT0 = 16'h555f;
    defparam counter_43_add_4_17.INIT1 = 16'h555f;
    defparam counter_43_add_4_17.INJECT1_0 = "NO";
    defparam counter_43_add_4_17.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_15 (.A0(counter[13]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[14]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n224), .COUT(n225), .S0(n117), .S1(n116));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_15.INIT0 = 16'h555f;
    defparam counter_43_add_4_15.INIT1 = 16'h555f;
    defparam counter_43_add_4_15.INJECT1_0 = "NO";
    defparam counter_43_add_4_15.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_13 (.A0(counter[11]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[12]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n223), .COUT(n224), .S0(n119), .S1(n118));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_13.INIT0 = 16'h555f;
    defparam counter_43_add_4_13.INIT1 = 16'h555f;
    defparam counter_43_add_4_13.INJECT1_0 = "NO";
    defparam counter_43_add_4_13.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_11 (.A0(counter[9]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[10]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n222), .COUT(n223), .S0(n121), .S1(n120));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_11.INIT0 = 16'h555f;
    defparam counter_43_add_4_11.INIT1 = 16'h555f;
    defparam counter_43_add_4_11.INJECT1_0 = "NO";
    defparam counter_43_add_4_11.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_9 (.A0(counter[7]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[8]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n221), .COUT(n222), .S0(n123), .S1(n122));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_9.INIT0 = 16'h555f;
    defparam counter_43_add_4_9.INIT1 = 16'h555f;
    defparam counter_43_add_4_9.INJECT1_0 = "NO";
    defparam counter_43_add_4_9.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_7 (.A0(counter[5]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[6]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n220), .COUT(n221), .S0(n125), .S1(n124));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_7.INIT0 = 16'h555f;
    defparam counter_43_add_4_7.INIT1 = 16'h555f;
    defparam counter_43_add_4_7.INJECT1_0 = "NO";
    defparam counter_43_add_4_7.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_5 (.A0(counter[3]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[4]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n219), .COUT(n220), .S0(n127), .S1(n126));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_5.INIT0 = 16'h555f;
    defparam counter_43_add_4_5.INIT1 = 16'h555f;
    defparam counter_43_add_4_5.INJECT1_0 = "NO";
    defparam counter_43_add_4_5.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_3 (.A0(counter[1]), .B0(GND_net), .C0(GND_net), 
          .D0(VCC_net), .A1(counter[2]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .CIN(n218), .COUT(n219), .S0(n129), .S1(n128));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_3.INIT0 = 16'h555f;
    defparam counter_43_add_4_3.INIT1 = 16'h555f;
    defparam counter_43_add_4_3.INJECT1_0 = "NO";
    defparam counter_43_add_4_3.INJECT1_1 = "NO";
    CCU2C counter_43_add_4_1 (.A0(GND_net), .B0(GND_net), .C0(GND_net), 
          .D0(GND_net), .A1(counter[0]), .B1(GND_net), .C1(GND_net), 
          .D1(VCC_net), .COUT(n218), .S1(n130));   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(10[18:29])
    defparam counter_43_add_4_1.INIT0 = 16'h0000;
    defparam counter_43_add_4_1.INIT1 = 16'h555f;
    defparam counter_43_add_4_1.INJECT1_0 = "NO";
    defparam counter_43_add_4_1.INJECT1_1 = "NO";
    PUR PUR_INST (.PUR(VCC_net));
    defparam PUR_INST.RST_PULSE = 1;
    LUT4 i1_4_lut_adj_6 (.A(n265), .B(n281), .C(n285), .D(n263), .Z(n297)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i1_4_lut_adj_6.init = 16'hfffe;
    LUT4 i1_4_lut_adj_7 (.A(counter[16]), .B(counter[23]), .C(counter[14]), 
         .D(counter[19]), .Z(n283)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i1_4_lut_adj_7.init = 16'hfffe;
    LUT4 i1_4_lut_adj_8 (.A(counter[17]), .B(counter[20]), .C(counter[10]), 
         .D(counter[8]), .Z(n285)) /* synthesis lut_function=(A+(B+(C+(D)))) */ ;   // c:/workspace/icesugarpro/blink-led-diamond/blink.v(5[9:25])
    defparam i1_4_lut_adj_8.init = 16'hfffe;
    
endmodule
//
// Verilog Description of module PUR
// module not written out since it is a black-box. 
//

